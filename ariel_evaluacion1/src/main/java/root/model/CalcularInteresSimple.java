
package root.model;


public class CalcularInteresSimple {
    
    public float interesSimple(float amount, float rate, float year) {
        float interesSimple = amount*(rate/100)*year;
        return interesSimple;
    }
    
}
